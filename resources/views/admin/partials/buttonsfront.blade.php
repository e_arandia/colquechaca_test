<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-4 col-md-8">
            <button type="submit" class="btn btn-primary">{{$label}}</button>
        </div>
    </div>
</div>
