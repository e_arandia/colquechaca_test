{{---------- Imagen --}}
<div class="form-group row">
    {!! Form::label('label_act', ucfirst(trans('common.previewact')), ['class' => 'col-md-4
    form-control-label text-md-right']) !!}
    <div class="col-md-8">
        <img src="{{ $collection?$collection->image_path:asset('img/no-image.png') }}" class="img-fluid w-20" />
    </div>
</div>
<div class="form-group row {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('label_insert', ucfirst(trans('common.image')), ['class' => 'col-md-4 form-control-label text-md-right']) !!}
    <div class="col-md-4 fileContainer">
        {!! Form::file('image', ['class' => 'col-md-6', 'id' =>
        'image','accept'=>"image/x-png,image/gif,image/jpeg"]) !!}
        {!! $errors->first('image_error', '<p class="help-block">:message</p>') !!}
        <span class="btn btn-success width">
            <i class="fas fa-images"></i>
            {{ucfirst(trans('common.select_image'))}}
        </span>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('result_preview', ucfirst(trans('common.preview')), ['class' => 'col-md-4 form-control-label text-md-right']) !!}
    <img src="" alt="" id="preview" class="img-fluid d-none prev-img">
</div>
{{------ end imagen ---------}}