<legend>
    <div class="row">
        <div class="col-lg-12">
            <fieldset>
                <div class="card card-primary card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">
                      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-three-user-data-tab" data-toggle="pill" href="#custom-tabs-three-user-data" role="tab" aria-controls="custom-tabs-three-user-data" aria-selected="true">
                                {{ ucfirst(trans('common.data')) }}
                            </a>
                        </li>
                      </ul>                  
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-three-tabContent">
                            <div class="form-group row {{ $errors->has('nombre') ? 'has-error' : ''}}">
                                <label for="name" class="col-md-4 form-control-label text-md-right">
                                    <span class="text-danger">*</span>
                                    {{ ucfirst(trans('common.name')) }}
                                </label>    
                                <div class="col-md-4">
                                    {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => ucfirst(trans('common.name'))]) !!}
                                    {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('descripcion') ? 'has-error' : ''}}">
                                <label for="descripcion" class="col-md-4 form-control-label text-md-right">
                                    <span class="text-danger">*</span>
                                    {{ ucfirst(trans('common.description')) }}
                                </label>
                                <div class="col-md-4">
                                    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => '...']) !!}
                                    {!! $errors->first('descripcion', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('cantidad_inicial') ? 'has-error' : ''}}">
                                <label for="email" class="col-md-4 form-control-label text-md-right">
                                    <span class="text-danger">*</span>
                                    {{ ucfirst(trans('common.initial_quantity')) }}
                                </label>
                                <div class="col-md-4">
                                    {!! Form::number('cantidad_inicial', null, ['class' => 'form-control', 'min'=>1]) !!}
                                    {!! $errors->first('cantidad_inicial', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                  </div>    
            </fieldset>
        </div>
    </div>
</legend>