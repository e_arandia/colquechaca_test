<div class="modal fade" id="modalDelete">
    <form action="" method="POST">
        {{ csrf_field() }}
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden>x</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="cantidad" class="col-md-4 form-control-label text-md-right">
                            <span class="text-danger">*</span>
                            {{ ucfirst(trans('common.quantity')) }}
                        </label>
                        <div class="col-md-4">
                            {!! Form::number('cantidad', null, ['class' => 'form-control', 'min'=>1, 'max'=>$max_qty]) !!}
                            {!! $errors->first('cantidad', '<p class="help-block">:message</p>') !!}
                        </div>                        
                    </div>

                    <div class="form-group row">
                        <label for="date" class="col-md-4 form-control-label text-md-right">
                            <span class="text-danger">*</span>
                            Fecha
                        </label>    
                        <div class="col-md-4">
                            <div class="input-group date" id="date" data-target-input="nearest">
                                {!! Form::text('date', null, ['class' => 'form-control datetimepicker-input', 'data-target' => '#date']) !!}
                                {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
                                <div class="input-group-append" data-target="#date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">                        
                        

                        <label for="motivo" class="col-md-4 form-control-label text-md-right">
                            Motivo:
                        </label>
                        <div class="col-md-4">
                            {!! Form::select('motivo', ['perdida'=>'Pérdida', 'fin_vida_util'=>'Fin vida útil', 'deshuso'=>'Deshuso'],null, ['class' => 'form-control col-md-12']) !!}
                            {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ ucwords(trans('common.cancel')) }}
                    </button>
                    <button type="submit" class="btn btn-primary">
                        {{ ucwords(trans('common.delete')) }}
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
