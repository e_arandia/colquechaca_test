@extends('layouts.admin.app')
@section('content')    
    <div class="row p-5">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
                <span class="info-box-icon bg-success elevation-1">
                    <i class="fas fa-users"></i>
                </span>
      
                <div class="info-box-content">
                    <span class="info-box-text">{{ ucfirst(trans('common.users')) }}</span>
                    <span class="info-box-number">
                    {{$register['users']}}
                    </span>
                </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
    </div>
@endsection