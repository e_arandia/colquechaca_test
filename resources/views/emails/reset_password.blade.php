<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base</title>
</head>
<body>
<h4>Reset password</h4>
  <p>
    Click here to reset your password: 
      <a style="color: #1c84c6;" href="{{ $link }}">
        Reset Password
      </a>
  </p>
  <p style="font-size:8px;">
    If link does not work copy and paste the url below: {{$link}}
  </p>
</body>
</html>