<!-- jQuery -->
<script src="{!! asset('cms/plugins/jquery/jquery.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('cms/plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{!! asset('cms/plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>
<!-- ChartJS -->
<script src="{!! asset('cms/plugins/chart.js/Chart.min.js') !!}"></script>
<!-- Select2 -->
<script src="{!! asset('cms/plugins/select2/js/select2.full.min.js') !!}"></script>
<!-- Sparkline -->
<script src="{!! asset('cms/plugins/sparklines/sparkline.js') !!}"></script>
<!-- JQVMap -->
<script src="{!! asset('cms/plugins/jqvmap/jquery.vmap.min.js') !!}"></script>
<script src="{!! asset('cms/plugins/jqvmap/maps/jquery.vmap.usa.js') !!}"></script>
<!-- jQuery Knob Chart -->
<script src="{!! asset('cms/plugins/jquery-knob/jquery.knob.min.js') !!}"></script>
<!-- daterangepicker -->
<script src="{!! asset('cms/plugins/moment/moment-with-locales.min.js') !!}"></script>
<script src="{!! asset('cms/plugins/daterangepicker/daterangepicker.js') !!}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{!! asset('cms/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
<!-- Summernote -->
<script src="{!! asset('cms/plugins/summernote/summernote-bs4.min.js') !!}"></script>
<!-- overlayScrollbars -->
<script src="{!! asset('cms/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('cms/dist/js/adminlte.js') !!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{!! asset('cms/dist/js/pages/dashboard.js') !!}"></script>
<!-- Validation -->
<script src="{!! asset('cms/dist/js/jquery-validation/jquery.validate.min.js') !!}"></script>
<script src="{!! asset('cms/dist/js/jquery-validation/additional-methods.min.js') !!}"></script>
<!-- Filter table by Column -->
<script src="{!! asset('cms/plugins/filter-table/jquery.columnfilters.js') !!}"></script>
<!-- Bootstrap Switch -->
<script src="{!! asset('cms/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}"></script>
<!-- custom js -->
<script src="{!! asset('cms/js/custom.js') !!}"></script>