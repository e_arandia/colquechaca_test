<!DOCTYPE html>
<html>
    @include('layouts.admin.header')

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            @include('layouts.admin.navbar')
            @include('layouts.admin.menu')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('layouts.admin.footer')
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
        </div>
        <script>
            // **************************************************************************************************
            // MAPA
            // **************************************************************************************************
            let marker;          //variable del marcador
            let coords = {};    //coordenadas obtenidas con la geolocalización
            
            //Funcion principal
            initMap = function () 
            {
                //usamos la API para geolocalizar el usuario
                    navigator.geolocation.getCurrentPosition(
                    function (position){
                        coords =  {
                        lng: position.coords.longitude,
                        lat: position.coords.latitude
                        };
                        setMapa(coords);  //pasamos las coordenadas al metodo para crear el mapa
                        
                    },function(error){console.log(error);});
            }
        
            function setMapa (coords)
            {   
                //Se crea una nueva instancia del objeto mapa
                var map = new google.maps.Map(document.getElementById('mapa'),
                {
                    zoom: 15,
                    center:new google.maps.LatLng(coords.lat,coords.lng),
        
                });
        
                //Creamos el marcador en el mapa con sus propiedades
                //para nuestro obetivo tenemos que poner el atributo draggable en true
                //position pondremos las mismas coordenas que obtuvimos en la geolocalización
                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(coords.lat,coords.lng),
        
                });
                //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
                //cuando el usuario a soltado el marcador
                marker.addListener('click', toggleBounce);
                marker.addListener( 'dragend', function (event)
                {
                    //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
                    document.getElementById("lat1").value = this.getPosition().lat();
                    document.getElementById("lng1").value = this.getPosition().lng();
                });
            }
        
            //callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
            function toggleBounce() {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }        
            }
        </script>
        @include('layouts.admin.script')
        @yield('js')

    </body>
</html>
           



