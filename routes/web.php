<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\ActivosController;
use App\Http\Controllers\Admin\BajarsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [AuthController::class,'loginform']);
Route::get('login', [AuthController::class,'loginform']);
Route::get('admin', [AuthController::class,'loginform']);
Route::post('login', [ 'as' => 'login', 'uses' => 'App\Http\Controllers\AuthController@login']);
Route::get('logout', [AuthController::class,'logout']);
Route::get('register', [AuthController::class,'registrationForm']);

Route::group(['middleware'=>['auth'], 'prefix' => 'admin'], function(){
    Route::get('dashboard', [DashboardController::class, 'index']);
    Route::resource('users', UsersController::class);
    Route::resource('activos', ActivosController::class);
    Route::resource('bajas', BajasController::class);
    Route::post('activos/{id}/dar_baja', [ActivosController::class,'dar_baja']);
    
});