<?php

namespace App\Http\Controllers\Admin;

use App\Helper;
use Carbon\Carbon;
use App\Models\Baja;
use App\Models\Activo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ActivosController extends Controller
{
    public function __construct()
    {
        view()->share('section','activos');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $lang = app()->getLocale();
        $activos = Activo::orderBy('id', 'ASC');
        
        $paginate = $request->pagination ? $request->pagination : 20;
        $page = (int)$request->page;
        if ($request->keyword != '')
            $activos = $activos->where('nombre', 'LIKE', '%' . $request->keyword . '%')
                ->orWhere('codigo', 'LIKE', $request->keyword . '%');

        $text_pagination = Helper::messageCounterPagination($activos->count(), $page, $paginate, $lang);

        $activos = $activos->paginate($paginate);

        return view('admin.activos.index', compact('activos', 'paginate', 'text_pagination'));
    }

    public function create()
    {
        return view('admin.activos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required|min:5',
            'cantidad_inicial' => 'required|numeric|min:0|not_in:0'
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $fields = $request->all();
        
        $code_tmp = 'CM';
        $qty_activos = Activo::get()->count();
        $fields['codigo'] = $code_tmp .str_pad($qty_activos ? ($qty_activos+1) :'1', 3, "0", STR_PAD_LEFT);
        $fields['stock'] = $request->cantidad_inical;
        $activo = Activo::create($fields);

        if ($activo) {
            Session::flash('flash_message', Helper::contentFlashMessage('create')['success']);
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', Helper::contentFlashMessage('create')['error']);
            Session::flash('flash_message_type', 'danger');
        }

        return redirect('admin/activos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activo = Activo::find($id);
        
        return view('admin.activos.edit', compact('activo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Activo = Activo::find($id);
        $fields = $request->all();

        $v = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required|min:5'
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $Activo = $Activo->update($fields);

        if ($activo) {
            Session::flash('flash_message', Helper::contentFlashMessage('update')['success']);
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', Helper::contentFlashMessage('update')['error']);
            Session::flash('flash_message_type', 'danger');
        }

        return redirect('admin/activos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $activo = Activo::findOrFail($id);

        if ($activo) {            
            $activo->delete(); //delete phisically   
            Session::flash('flash_message', Helper::contentFlashMessage('delete')['success']);
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', Helper::contentFlashMessage('delete')['error']);
            Session::flash('flash_message_type', 'danger');
        }
        return redirect('admin/activos');
    }

    public function dar_baja(Request $request,$id)
    {
        $fields = $request->all();
        $v = Validator::make($request->all(), [
            'date'=>'required',
            'motivo' => 'required',
            'cantidad'=>'required|numeric'
        ]);
        if ($v && $v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      
        $fields['fecha'] = Helper::date_database($request->date);
        $fields['activo_id'] = $id;

        $baja = Baja::create($fields);
        $activo = Activo::find($id);
        $stock = $activo->cantidad_inicial - $request->cantidad;
        $activo->update(['stock'=>$stock]);
        if ($baja) {
            Session::flash('flash_message', Helper::contentFlashMessage('create')['success']);
            Session::flash('flash_message_type', 'success');
        } else {
            Session::flash('flash_message', Helper::contentFlashMessage('create')['error']);
            Session::flash('flash_message_type', 'danger');
        }

        return redirect('admin/activos');
    }
}
